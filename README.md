# Token tool

Set up a token to read custom images and package registry items (such as files containing constants or scripts) in a CICD library.

**Don't stress!** We are only granting access to read private published content, and the deploy token remains valid as long as you designate.

1. In GitLab, navigate to the private project where the image is registereed (such as a CICD library project)
1. Open Settings > Repository > Deploy tokens > Add token
1. Create a token
    - Name it `read-token`
    - Grant `read_registry` access
    - Grant `read_package_registry` access
    - Optionally give the token an expiration date
1. Store the username and token in a password manager such as 1Password
1. In GitLab, navigate to the Group containing all the projects that need access
1. Open Settings > CI/CD > Variables > Add variable
1. Add the variable for package registry access
    - Uncheck "Protect variable"
    - Check "Mask variable"
    - Uncheck "Expand variable reference"
    - Give it the key `CICD_LIBRARY_TOKEN`
    - Paste in the tooken
1. Encode the token for custom image use.
    1. Clone this repo and CD to it
    1. On your local machine in the repo, run `./encode | pbcopy` (assuming MacOS)
    1. Copy and paste the username and token from above into the script (the `pbcopy` part will copy the output to the pastebin for you)
1. Back in GitLab, add another variable for custom image registry access
    - Uncheck and check the same settings as for the previous variable
    - Give it the key `CICD_LIBRARY_TOKEN_ENCODED`
    - Paste the value from the `encode` script
1. In any projects that need to use the image, add the YAML below (OK to use `include:`)
    ```yaml
    variables:
        DOCKER_AUTH_CONFIG: >
        {
            "auths": {
            "$CI_REGISTRY": {
                "auth": "$CICD_LIBRARY_TOKEN_ENCODED"
                }
            }
        }
    image: $CI_REGISTRY/path/to/library/container:latest
    ```
